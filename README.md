# Machine Learning Library


### About
Questo repository contiene alcuni modelli e algoritmi di Machine Learning che ho avuto modo di studiare.
Lo scopo non è tanto quello di produrre algoritmi ottimizzati quanto più possibile per essere efficienti dal punto di vista
computazionale, quanto come materiale per lo studio personale e la divulgazione di questi codici tra colleghi e user groups.

Per contattarmi, potete scrivere al mio indirizzo [email](mailto:alessandro.cucci@gmail.com)
o visitare il mio [sito web](http://www.alessandrocucci.it).


### Installazione
    pip install -r requirements.txt


### Tools
- [Statistics for Python 2.6+](utils/statistics.py)

### IPython/Jupyter Notebooks
TODO

## Implementazioni
#### Supervised Learning:
- [Natural Language Naive Bayes](supervised/naivebayes.py)
- [Neural Network](supervised/neural_network.py)

#### Unsupervised Learning:
TODO
