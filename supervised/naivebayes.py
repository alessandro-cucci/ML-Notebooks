#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import defaultdict
import math
import re
import string

from utils.nl_constants import cachedStopWords


class TextNaiveBayesClassifier:
    """
    Classificatore Bayesiano Naive (ingenuo)

    La classe si instanzia in questo modo:

    >>> classifier = TextNaiveBayesClassifier(k=0.5)

    dove k e' lo pseudo-contatore usato per la funzione di smoothing per evitare
    i casi di underflow causati moltiplicando numeri di tipo float prossimi allo 0.


    I metodi principali lato utente sono due:
    - train: per istruire il classificatore
    - classify: con il quale dato un testo si cerchera' di individuarne la classe

    >>> training_set = [("Un testo", "positivo"), ("Altro testo", "negativo"), ]
    >>> classifier.train(training_set)
    >>> classifier.classify("Messaggio di cui voglio stimare la classe")

    Il resto dei metodi, a supporto di questi due principali, sono:
    - bagOfWords: data una stringa torna una lista di parole da analizzare
    - count_words: dato un "training set" conta per ogni parola quante volte
                    compare in testi positivi e quanti in testi negativi
    - word_probabilities: tramite una funzione di smoothing ci dice le probabilita'
                            che un testo contenente quella parola sia positivo o negativo
    - positive_probability: qui applichiamo il teorema di Bayes, per trovare la percentuale
                            che un il sentimento del testo sia positivo.
    """

    regex = re.compile('[%s]' % re.escape(string.punctuation))

    def __init__(self, k=0.5):
        self.k = k
        self.word_probs = []

    def bagOfWords(self, stringa):
        """
        Partiziona la stringa in un elenco di parole.
        rimuove spazi, caratteri di punteggiatura,
        url, stop words, menzioni (@) e 'RT'.
        Trasforma tutte le maiuscole in minuscole.
        """
        words = stringa.lower().strip().split(' ')
        for word in words:
            word = word.rstrip().lstrip()
            if not re.match(r'^https?:\/\/.*[\r\n]*', word) \
            and not re.match(r'^http?:\/\/.*[\r\n]*', word) \
            and not re.match('^@.*', word) \
            and not re.match('\s', word) \
            and word not in cachedStopWords \
            and word != 'rt' \
            and word != '':
                yield self.regex.sub('', word)

    def count_words(self, training_set):
        """
        Metodo per contare dal nostro training set
        per ogni parola quante volte appare in un messaggio positivo e quante volte
        in uno negativo.
        Restituisce un dizionario con chiavi le parole e valori una lista
        di due elementi (conteggio per i positivi, e conteggio per i negativi),
        es.

        counts = {
            'ciao': [38, 29],
            'male': [3, 59],
            'bellissimo': [99, 4],
            ...
        }
        """
        counts = defaultdict(lambda: [0, 0])
        for message, is_positive in training_set:
            for word in self.bagOfWords(message):
                counts[word][0 if is_positive else 1] += 1
        return counts

    def word_probabilities(self, counts, total_positives, total_non_positives):
        """
        Funzione di smooting.
        Ritorna una lista di tuple (di tre elementi) per ogni parola.

        [(parola, p(parola | positiva), p(parola | negativa)
        """
        return [(w,
                 (positive + self.k) / (total_positives + 2 * self.k),
                 (non_positive + self.k) / (total_non_positives + 2 * self.k))
                 for w, (positive, non_positive) in counts.iteritems()]

    def positive_probability(self, word_probs, message):
        """
        Calcola la probabilita' che il messaggio sia positivo.
        """
        message_words = list(self.bagOfWords(message))
        log_prob_if_positive = 0.0
        log_prob_if_not_positive = 0.0

        for word, prob_if_positive, prob_if_not_positive in word_probs:

            if word in message_words:
                log_prob_if_positive += math.log(prob_if_positive)
                log_prob_if_not_positive += math.log(prob_if_not_positive)

            else:
                log_prob_if_positive += math.log(1.0 - prob_if_positive)
                log_prob_if_not_positive += math.log(1.0 - prob_if_not_positive)

        prob_if_positive = math.exp(log_prob_if_positive)
        prob_if_not_positive = math.exp(log_prob_if_not_positive)
        positive = (prob_if_positive / (prob_if_positive + prob_if_not_positive))
        positive_text = ":)" if (positive * 100) >= 50 else ":("
        return positive_text, positive

    def train(self, training_set):
        # conto i messaggi positivi e quelli negativi
        num_positives = len([is_positive
                         for message, is_positive in training_set
                         if is_positive])
        num_non_positives = len(training_set) - num_positives

        # costruisco il mio dizionario di termini
        word_counts = self.count_words(training_set)
        self.word_probs = self.word_probabilities(word_counts,
                                             num_positives,
                                             num_non_positives)

    def classify(self, message):
        return self.positive_probability(self.word_probs, message)